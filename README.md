# README #
### Para que serve esse repositório? ###

Esse repositório foi criado como parte de um projeto da disciplina de Interfaces Homem-Máquina.
A ideia é criar uma plataforma onde as pessoas podem ver fotos de gato e favoritar as que mais gostam.
Também há a possibilidade de postar fotos de seu próprio gato.

### Como funciona a aplicação? ###

As fotos serão identificadas com tags. 
Ao entrar na página inicial do aplicativo, o usuário verá uma mensagem perguntando o que ele gostaria de ver hoje, juntamente de uma caixa de pesquisa onde ele pode pesquisar por tags.
Dentro das tags, as fotos podem ser ordenadas por ordem de postagem (default) ou por popularidade.
Na página inicial, também serão mostradas algumas fotos escolhidas aleatoriamente.
Para evitar a postagem de conteúdos inapropriados, as imagens postadas passarão por moderação